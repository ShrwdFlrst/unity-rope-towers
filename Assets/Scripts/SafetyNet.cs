﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class SafetyNet : MonoBehaviour {

	private GameObject player;
	private GameObject start;
	private Transform startingPoint;

	void OnTriggerEnter(Collider other) {

		if (other.name == player.name) {
			player.GetComponent<FirstPersonController> ().enabled = false;
			player.transform.position = startingPoint.position;
			player.transform.rotation = startingPoint.rotation;
			player.GetComponent<FirstPersonController> ().enabled = true;
		}

//		player = GameObject.Find (other.name);
//
//		Debug.Log (player.GetComponent<Rigidbody> ().velocity.ToString());
//		player.GetComponent<Rigidbody> ().velocity = Vector3.zero;
//		player.GetComponent<Rigidbody> ().velocity = Vector3.zero;
//
//		Debug.Log (player.transform.position);
//		Debug.Log (startingPoint.position);
//
//		player.transform.position = startingPoint.position;
//		Debug.Log (player.transform.position);
	}

	void Update() {
		Debug.Log (startingPoint.transform.position);
		Debug.Log (player.transform.position);
	}

	void Start()
	{
		player = GameObject.Find ("FPSController");
		start = GameObject.Find ("Start Position");
		if (start != null && startingPoint == null) {
			startingPoint = start.transform;
		}
	}

}
