﻿using UnityEngine;
using System.Collections;

public class RopeJoint : MonoBehaviour {
	
	bool  parent;
	Transform connectTo;
	
	void  Start() {
		
		gameObject.AddComponent<CharacterJoint>();
		CharacterJoint joint = GetComponent<CharacterJoint> ();

		SoftJointLimit limit = new SoftJointLimit();
		limit.limit = 1;

		joint.swing1Limit = limit;
		joint.swing2Limit = limit;

		if(!parent) { 
			joint.connectedBody=transform.parent.GetComponent<Rigidbody>();
			transform.parent = null; 
		}
		
	}
	
	void  Update() {
		if(parent && connectTo != null) {
			transform.position=connectTo.position;
		}
	}
	
}